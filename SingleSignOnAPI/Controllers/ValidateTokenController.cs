﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SingleSignOnAPI.Models;
using SingleSignOnAPI.Interfaces;
namespace SingleSignOnAPI.Controllers
{
    public class ValidateTokenController : ApiController, IsSOAuthentication
    {
        private long _ssoSystemID;
        public SSOSystemToken sSOToken { get; set; }
        // GET api/<controller>
        [HttpGet]
        public object CheckAuthorizedToken(string userToken)
        {
            try
            {
                _ssoSystemID = sSOToken.sSOSystemID;
                using (ebnpartnerportal_prodEntities ebnDB = new ebnpartnerportal_prodEntities())
                {
                    if (userToken != null)
                    {
                        UserProfile userProfile = ebnDB.UserProfiles.Where(c => c.UserToken == userToken).FirstOrDefault();
                        if (userProfile != null)
                        {
                            return new { UserName = userProfile.UserName, Email = userProfile.Email, FullName = userProfile.FullName };
                        }
                        else
                        {
                            return new { Status = "Failed", Message = "Invalid user token" };
                        }
                    }
                    else
                    {
                        return new { Status = "Failed", Message = "Invalid user token" };
                    }
                }
            }
            catch(Exception ex)
            {
                return new { Status = "Failed", Message = ex.Message };
            }

        }
    }
}