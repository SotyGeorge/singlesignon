﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SingleSignOnAPI.Interfaces;
using System.Collections.Specialized;
using SingleSignOnAPI.Models;

namespace SingleSignOnAPI.Controllers
{
    public class GenerateTokenController : ApiController 
    {

        // GET api/<controller>
        public object getToken(long sSOSystemID)
        {
            try
            {
                SSOSystemToken token = new SSOSystemToken();

                token.sSOSystemID = sSOSystemID;

                return token.GenerateToken();
            }
            catch(Exception ex)
            {
                return new { Status = "Failed", Message = ex.Message };
            }
        }

    }
}