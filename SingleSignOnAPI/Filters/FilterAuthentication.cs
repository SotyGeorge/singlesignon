﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SingleSignOnAPI.Interfaces;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using Newtonsoft.Json.Linq;
using SingleSignOnAPI.Models;

namespace SingleSignOnAPI.Filters
{
    public class FilterAuthentication : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
       
            var sSOAuth = actionContext.ControllerContext.Controller as Interfaces.IsSOAuthentication;
            if (sSOAuth == null) return;

            var authorization = actionContext.Request.Headers.Authorization;
            if (authorization == null) throw new System.IdentityModel.Tokens.SecurityTokenException("Authentication failed.");
            if (string.Compare(authorization.Scheme, "eBNSSO", true) != 0) throw new System.IdentityModel.Tokens.SecurityTokenException("Authentication failed.");



            sSOAuth.sSOToken = SSOSystemToken.ParseToken(authorization.Parameter);
        }
    }
}