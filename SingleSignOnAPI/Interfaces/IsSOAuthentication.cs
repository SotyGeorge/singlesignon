﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;
using SingleSignOnAPI.Models;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using System.Collections.Specialized;
namespace SingleSignOnAPI.Interfaces
{
    interface IsSOAuthentication
    {
        SSOSystemToken sSOToken { get; set; }
    }
  
}