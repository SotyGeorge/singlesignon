﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;

namespace SingleSignOnAPI.Models
{
    public class SSOSystemToken
    {
        public long sSOSystemID { get; set; }
        internal const string FAULT_EXCEPTION_MESSAGE = "Authentication failed.";
        internal const string COULDNOT_GENERATE_EXCEPTION_MESSAGE = "Failed to generate a key.";

        private static void WrietLongToBuffer(byte[] buffer, ref int index, long value)
        {
            for (var i = 0; i < 8; i++)
            {
                buffer[index] = (byte)((value >> (i * 8)) & 0xFF);
                index++;
            }

        }
        private static long ReadLongFromBuffer(byte[] buffer, ref int index)
        {
            var value = 0L;
            long tempValue = 0L;
            for (var i = 0; i < 8; i++)
            {
                tempValue = buffer[index];
                value |= (tempValue & 0xFF) << (i * 8);
                index++;
            }
            return value;
        }
        private static byte[] EncryptData(byte[] secretData)
        {
            var rsaCryptoServiceProvider = new RSACryptoServiceProvider();
            rsaCryptoServiceProvider.FromXmlString("<RSAKeyValue><Modulus>kFqqx6bomtqmvhiSobhWqHpdkAK0grMEV/FdpX6N5TpDRWaD27haJdhJozjM/pNmBWnGfCFgVy49+iArlf5XMXl8rnL9izfHuzFSr/zbnf+mMlLZqxpmGQg6HO3HiHVj5cjlG52r5PB/rrSFEjV0oDaXjH4j6gpC5tJICOA9Lx8=</Modulus><Exponent>AQAB</Exponent><P>yWa1gwZH8GaGhIwxvwdT9ydShDJgWHJJagjpaz3UKr7FMZDHFGSvsgxcJaQF4DQxbrVx2jdlbtm1if1PYKMeXw==</P><Q>t3zhVi2nt/JsGWs0i4Z5H0+kfu7oAclZnYJ5KhipuTHM3msslZzIOM0h6KVQ6O9c8sN5RPXm9oaUg+hLrA4nQQ==</Q><DP>wwS1ll4qotpkP00Rjoyl/ZkSCfhN2tcvx4FBpRqFq652e/xZCaJFjv7w63HcTrG7fBwuVsN1cNVXOHsUtdq9uQ==</DP><DQ>p9WYoCU+pmkeK9n9xCoKnHNS+bA5k3jDeemgPrs0c+tzg3bw3yD7m8k23QBqE8budDgMsuFik9jh/A39ObHwgQ==</DQ><InverseQ>qfT0qUgprsf4RoOoN8YBBFtGnZbvG1GvBhKigDzVtlGsKkXlhhdKyR3JZZ1AWR2WsmQlT287hDk/l4JzfcOz8g==</InverseQ><D>dOnp/Y/SPnEusTHHuNFK5mNM2fFG78A7mVp0ZTAtjmV0zIWt78vMv3AAnADKDrmk3GeCCVEi7RkXuzhI9M+tH74xh+rXXXV63VpziimuWrAxv9DeXqSS/0WoPmaZD10kUb+mnuP7uRblNeu83xxoQJAiocH+7Uj4/M0VhyvZ2oE=</D></RSAKeyValue>");
            var lastBlockLength = secretData.Length % 58;
            var blockCount = secretData.Length / 58;
            var hasLastBlock = false;
            if (lastBlockLength != 0)
            {
                blockCount += 1;
                hasLastBlock = true;
            }
            var result = new byte[blockCount * 128];
            var currentBlock = new byte[58];
            for (var blockIndex = 0; blockIndex <= blockCount - 1; blockIndex++)
            {
                int thisBlockLength;

                //If this is the last block and we have a remainder, 
                //then set the length accordingly

                if (blockCount == blockIndex + 1 && hasLastBlock)
                {
                    thisBlockLength = lastBlockLength;
                    currentBlock = new byte[lastBlockLength];
                }
                else
                    thisBlockLength = 58;

                var startChar = blockIndex * 58;

                //Define the block that we will be working on
                Array.Copy(secretData, startChar, currentBlock, 0, thisBlockLength);

                //Encrypt the current block and append it to the result stream
                var encryptedBlock = rsaCryptoServiceProvider.Encrypt(currentBlock, false);

                var originalResultLength = 128 * blockIndex; //result.Length;

                encryptedBlock.CopyTo(result, originalResultLength);
            }

            return result;
        }

        private static byte[] DecryptData(byte[] encryptedData)
        {
            var rsaCryptoServiceProvider = new RSACryptoServiceProvider();
            rsaCryptoServiceProvider.FromXmlString("<RSAKeyValue><Modulus>kFqqx6bomtqmvhiSobhWqHpdkAK0grMEV/FdpX6N5TpDRWaD27haJdhJozjM/pNmBWnGfCFgVy49+iArlf5XMXl8rnL9izfHuzFSr/zbnf+mMlLZqxpmGQg6HO3HiHVj5cjlG52r5PB/rrSFEjV0oDaXjH4j6gpC5tJICOA9Lx8=</Modulus><Exponent>AQAB</Exponent><P>yWa1gwZH8GaGhIwxvwdT9ydShDJgWHJJagjpaz3UKr7FMZDHFGSvsgxcJaQF4DQxbrVx2jdlbtm1if1PYKMeXw==</P><Q>t3zhVi2nt/JsGWs0i4Z5H0+kfu7oAclZnYJ5KhipuTHM3msslZzIOM0h6KVQ6O9c8sN5RPXm9oaUg+hLrA4nQQ==</Q><DP>wwS1ll4qotpkP00Rjoyl/ZkSCfhN2tcvx4FBpRqFq652e/xZCaJFjv7w63HcTrG7fBwuVsN1cNVXOHsUtdq9uQ==</DP><DQ>p9WYoCU+pmkeK9n9xCoKnHNS+bA5k3jDeemgPrs0c+tzg3bw3yD7m8k23QBqE8budDgMsuFik9jh/A39ObHwgQ==</DQ><InverseQ>qfT0qUgprsf4RoOoN8YBBFtGnZbvG1GvBhKigDzVtlGsKkXlhhdKyR3JZZ1AWR2WsmQlT287hDk/l4JzfcOz8g==</InverseQ><D>dOnp/Y/SPnEusTHHuNFK5mNM2fFG78A7mVp0ZTAtjmV0zIWt78vMv3AAnADKDrmk3GeCCVEi7RkXuzhI9M+tH74xh+rXXXV63VpziimuWrAxv9DeXqSS/0WoPmaZD10kUb+mnuP7uRblNeu83xxoQJAiocH+7Uj4/M0VhyvZ2oE=</D></RSAKeyValue>");

            var maxBytes = encryptedData.Length;

            // 'Ensure that the length of the encrypted stream is divisible by 128
            if (maxBytes % 128 != 0)
            {
                //throw new System.Security.Cryptography.CryptographicException("Encrypted", "text is an invalid length");
                return null;
            }


            // 'Calculate the number of blocks we will have to work on
            var blockCount = maxBytes / 128;

            // 'Initialize the result buffer

            var result = new byte[(blockCount - 1) * 58];
            var currentBlockBytes = new byte[128];

            for (int blockIndex = 0; blockIndex <= blockCount - 1; blockIndex++)
            {
                Array.Copy(encryptedData, blockIndex * 128, currentBlockBytes, 0, 128);
                var currentBlockDecrypted = rsaCryptoServiceProvider.Decrypt(currentBlockBytes, false);
                var originalResultLength = blockIndex * 58; //result.Length;
                if (blockIndex == blockCount - 1)
                    Array.Resize(ref result, originalResultLength + currentBlockDecrypted.Length);
                currentBlockDecrypted.CopyTo(result, originalResultLength);
            }
            return result;
        }
        public static SSOSystemToken ParseToken(string tokenString)
        {
            try
            {
                var buffer = Convert.FromBase64String(tokenString);
                buffer = DecryptData(buffer);

                SSOSystemToken token = new SSOSystemToken();
                //SSOSystem dBSystem = null;
                var index = 0;
                token.sSOSystemID = ReadLongFromBuffer(buffer, ref index);
                return token;
            }
            catch
            {
                throw new Exception(FAULT_EXCEPTION_MESSAGE);
            }
        }

        public string GenerateToken()
        {
            var buffer = new byte[32];
            var index = 0;
            try
            {
                
                WrietLongToBuffer(buffer, ref index, sSOSystemID);

                buffer = EncryptData(buffer);
                using (ebnpartnerportal_prodEntities ebnDB = new ebnpartnerportal_prodEntities())
                {
                    if (ebnDB.ExternalSystems.Where(s => s.SystemID == sSOSystemID).Count() == 0)
                    {
                        throw new Exception(COULDNOT_GENERATE_EXCEPTION_MESSAGE);
                    }
                }
                return Convert.ToBase64String(buffer);
            }
            catch(Exception ex)
            {
                throw ex;
            }


        }


    }
}